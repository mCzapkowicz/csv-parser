
CREATE TABLE IF NOT EXISTS PointSQL (
    id SERIAL PRIMARY KEY,
    geometry GEOMETRY,
    ELEV int
);

CREATE TABLE IF NOT EXISTS LineSQL (
    id SERIAL PRIMARY KEY,
    geometry GEOMETRY,
    ELEV int
);

CREATE TABLE IF NOT EXISTS GeoSQL (
    id SERIAL PRIMARY KEY,
    geometry GEOMETRY,
    ELEV int
);

insert into LineSQL(geometry,ELEV) values(1, ST_GeomFromText('LINESTRING(0 0, 1 2, 2 3, 3 1)'),13);
