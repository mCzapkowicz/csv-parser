import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.lang.Object;

//import org.geotools.data.DataStoreFactorySpi;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import org.apache.commons.io.FileUtils;
import org.geotools.data.DataUtilities;
import org.geotools.data.DefaultTransaction;
//import org.geotools.data.FeatureStore;
import org.geotools.data.Transaction;
import org.geotools.data.collection.ListFeatureCollection;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;
import org.geotools.data.simple.SimpleFeatureCollection;
//import org.geotools.feature.FeatureCollection;
//import org.geotools.feature.FeatureCollections;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.geometry.jts.JTSFactoryFinder;
//import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

//import org.geotools.feature.simple.SimpleFeatureBuilder;
//import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.swing.data.JFileDataStoreChooser;
import org.geotools.data.simple.SimpleFeatureStore;
import org.geotools.data.simple.SimpleFeatureSource;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
//import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
//import com.vividsolutions.jts.geom.LineString;

public class Main extends Application
{

    public static void main(String[] args) throws Exception{
        launch(args);


        //String test = "((-300120 7689960, -654360 7562040, 1640 7512840))";
        //String geometry = test.replaceAll("[()]","");

        //System.out.println(geometry);


    }
    static String geom;
    static SimpleFeatureType TYPE;
    Stage window;

    @Override
    public void start(Stage primaryStage) throws Exception{
        window=primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        window.setTitle("File Converter");
        window.setScene(new Scene(root, 318, 300));
        root.getStylesheets().add("style.css");
        window.show();

    }

    @FXML
    private Button shape;

    @FXML
    private Button sql;

    @FXML
    private void SQLClicked(ActionEvent event){
        File fileCSVParser = JFileDataStoreChooser.showOpenFile("csv",null);
        if(fileCSVParser == null) System.exit(1);
        List<List<String>> ParsedList = CSVParser(fileCSVParser.getAbsolutePath());
        String querySQL = SqlQueryCreator(ParsedList);
        SaveQueryToFile(querySQL);
    }

    @FXML
    private void ShapeClicked(ActionEvent event) throws Exception
    {

        File file = JFileDataStoreChooser.showOpenFile("csv",null);
        if(file == null) System.exit(1);

        java.util.List<SimpleFeature> list = new ArrayList<SimpleFeature>();
        BufferedReader reader = new BufferedReader(new FileReader(file));
        TYPE = null;
        try {
            String line = reader.readLine();

            TYPE = createFeatureType2(line);

            GeometryFactory factory = JTSFactoryFinder.getGeometryFactory(null);

            for (line = reader.readLine(); line != null; line = reader.readLine()) {
                Object[] o = CreateObject(line,factory);
                SimpleFeature feature = SimpleFeatureBuilder.build(TYPE, o, null);
                list.add(feature);

            }

        } finally {
            reader.close();
        }
        File newFile = getNewShapeFile(file);

        ShapefileDataStoreFactory dataStoreFactory = new ShapefileDataStoreFactory();

        Map<String, Serializable> params = new HashMap<>();
        params.put("url", newFile.toURI().toURL());
        params.put("create spatial index", Boolean.TRUE);

        ShapefileDataStore newDataStore = (ShapefileDataStore) dataStoreFactory.createNewDataStore(params);

        /*
         * TYPE is used as a template to describe the file contents
         */
        newDataStore.createSchema(TYPE);

        /*
         * Write the features to the shapefile
         */
        Transaction transaction = new DefaultTransaction("create");

        String typeName = newDataStore.getTypeNames()[0];
        SimpleFeatureSource featureSource = newDataStore.getFeatureSource(typeName);
        SimpleFeatureType SHAPE_TYPE = featureSource.getSchema();
        /*
         * The Shapefile format has a couple limitations:
         * - "the_geom" is always first, and used for the geometry attribute name
         * - "the_geom" must be of type Point, MultiPoint, MuiltiLineString, MultiPolygon
         * - Attribute names are limited in length
         * - Not all data types are supported (example Timestamp represented as Date)
         *
         * Each data store has different limitations so check the resulting SimpleFeatureType.
         */
        System.out.println("SHAPE:"+SHAPE_TYPE);

        if (featureSource instanceof SimpleFeatureStore)
        {
            SimpleFeatureStore featureStore = (SimpleFeatureStore) featureSource;
            /*
             * SimpleFeatureStore has a method to add features from a
             * SimpleFeatureCollection object, so we use the ListFeatureCollection
             * class to wrap our list of features.
             */
            SimpleFeatureCollection collection = new ListFeatureCollection(TYPE, list);
            featureStore.setTransaction(transaction);
            try {
                featureStore.addFeatures(collection);
                transaction.commit();
            } catch (Exception problem) {
                problem.printStackTrace();
                transaction.rollback();
            } finally {
                transaction.close();
            }
            System.exit(0); // success!
        } else {
            System.out.println(typeName + " does not support read/write access");
            System.exit(1);
        }

    }



    private static File getNewShapeFile(File csvFile) {
        String path = csvFile.getAbsolutePath();
        String newPath = path.substring(0, path.length() - 4) + ".shp";

        JFileDataStoreChooser chooser = new JFileDataStoreChooser("shp");
        chooser.setDialogTitle("Save shapefile");
        chooser.setSelectedFile(new File(newPath));

        int returnVal = chooser.showSaveDialog(null);

        if (returnVal != JFileDataStoreChooser.APPROVE_OPTION) {
            // the user cancelled the dialog
            System.exit(0);
        }

        File newFile = chooser.getSelectedFile();
        if (newFile.equals(csvFile)) {
            System.out.println("Error: cannot replace " + csvFile);
            System.exit(0);
        }

        return newFile;
    }

    private static Object[] CreateObject(String line, GeometryFactory factory)//,
    //BufferedReader reader,
    //java.util.List<SimpleFeature> list,
    //SimpleFeature TYPE) throws Exception
    {
        //GeometryFactory factory = JTSFactoryFinder.getGeometryFactory(null);

        //for (line = reader.readLine(); line != null; line = reader.readLine()) {
        Object[] o = null;

        if(geom == "the_geom:Point") {
            String split[] = line.split("\\;");

            //String name = split[0];
            double latitude = Double.parseDouble(split[0]);
            double longitude = Double.parseDouble(split[1]);

            //System.out.println("" + latitude + " " + longitude);
            o = new Object[split.length + 1];
            //Object[] o = new Object[2];

            for (int i = 1; i < o.length; i++) {
                o[i] = split[i - 1];
            }

            o[0] = factory.createPoint(new Coordinate(longitude, latitude));
            //o[1] = split[2];
            //o[1] = name;

            //SimpleFeature feature = SimpleFeatureBuilder.build(TYPE,o,null);
            //list.add(feature);


            //}

        }
        else if(geom == "the_geom:Polygon")
        {
            String split[] = line.split("\\;");
            String geometry = split[0].replaceAll("[()]","");
            o = new Object[split.length];
            String[] points = geometry.split(",");

            Coordinate[] coordinates = new Coordinate[points.length];

            for(int i = 0; i<points.length; i++)
            {
                points[i] = points[i].replaceAll("^\\s+", "");

                String[] coords = points[i].split(" ");

                double x = Double.parseDouble(coords[0]);
                double y = Double.parseDouble(coords[1]);
                coordinates[i] = new Coordinate(x,y);
            }

            Polygon poly = factory.createPolygon(coordinates);
            Polygon[] a = new Polygon[1];
            a[0] = poly;
            o[0] = factory.createMultiPolygon(a);
            o[1] = split[1];


            //System.out.println(geometry);
            System.out.println(split[1]);
        }
        else if(geom == "the_geom:LineString")
        {
            String split[] = line.split("\\;");
            String geometry = split[0].replaceAll("[()]","");
            o = new Object[split.length];
            String[] points = geometry.split(",");

            Coordinate[] coordinates = new Coordinate[points.length];

            for(int i = 0; i<points.length; i++)
            {
                points[i] = points[i].replaceAll("^\\s+", "");

                String[] coords = points[i].split(" ");

                double x = Double.parseDouble(coords[0]);
                double y = Double.parseDouble(coords[1]);
                coordinates[i] = new Coordinate(x,y);
            }

            //Polygon poly = factory.createPolygon(coordinates);
            //Polygon[] a = new Polygon[1];
            //a[0] = poly;
            o[0] = factory.createLineString(coordinates);
            o[1] = split[1];


            //System.out.println(geometry);
            System.out.println(split[1]);
        }


        return o;
    }

    /*
    private static SimpleFeatureType createFeatureType(String[] headers) {

        SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
        builder.setName("Location");
        builder.setCRS(DefaultGeographicCRS.WGS84); // <- Coordinate reference system

        if(headers[0] == "the_geom:Polygon") builder.add("the_geom", Polygon.class);
        else if(headers[0] == "the_geom:LineString") builder.add("the_geom", LineString.class);
        // add attributes in order
        else builder.add("the_geom", Point.class);
        builder.length(15).add("Name", String.class); // <- 15 chars width for name field
        builder.add("number",Integer.class);

        // build the type
        final SimpleFeatureType LOCATION = builder.buildFeatureType();


        //stringBuilder.append("the_geom:Geometry,");

        for (String header : headers) {
            //System.out.println(header);
            //stringBuilder.append("").append(header).append(":String,");

        }

        //System.out.println(stringBuilder);

        //TYPE = DataUtilities.createType("Location", stringBuilder.substring(0, stringBuilder.toString().length() - 1));

        return LOCATION;
    }
    */

    private static SimpleFeatureType createFeatureType2(String line) throws Exception
    {
        StringBuilder stringBuilder = new StringBuilder();




        String[] headers = line.split("\\;");
        System.out.println(geom);
        if(headers[0].equals("the_geom:Polygon")) {
            //System.out.println("bomba");
            geom = "the_geom:Polygon";
        }
        else if(headers[0].equals("the_geom:LineString")) {

            geom = "the_geom:LineString";
        }
        else {
            geom = "the_geom:Point";

        }
        System.out.println(geom + " " + headers[0]);
        //System.out.println(headers[0]);
        stringBuilder.append(geom + ",");

        for (String header : headers) {
            if(header == headers[0]) continue;
            System.out.println(header);
            stringBuilder.append("").append(header).append(":String,");
        }
        System.out.println(stringBuilder);
        //System.out.println(stringBuilder);

        SimpleFeatureType TYPE = DataUtilities.createType("Location", stringBuilder.substring(0, stringBuilder.toString().length() - 1));


        return TYPE;
    }

    public static List<List<String>> CSVParser(String path)
    {
        String csvFile = path;
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ";";
        List<List<String>> listToParse = new ArrayList<List<String>>();
        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null)
            {
                List<String> arrayTemp =new ArrayList<>();
                String[] country = line.split(cvsSplitBy);
                for(int i = 0;i<country.length;i++)
                {
                    arrayTemp.add(country[i]);
                }
                listToParse.add(arrayTemp);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return listToParse;
    }

    public static String SqlQueryCreator(List<List<String>> ParsedList)
    {
        String sqlCommand = "\n" +
                "CREATE TABLE IF NOT EXISTS PointSQL (\n" +
                "    id SERIAL PRIMARY KEY,\n" +
                "    geometry GEOMETRY,\n" +
                "    ELEV int\n" +
                ");\n" +
                "\n" +
                "CREATE TABLE IF NOT EXISTS LineSQL (\n" +
                "    id SERIAL PRIMARY KEY,\n" +
                "    geometry GEOMETRY,\n" +
                "    ELEV int\n" +
                ");\n" +
                "\n" +
                "CREATE TABLE IF NOT EXISTS GeoSQL (\n" +
                "    id SERIAL PRIMARY KEY,\n" +
                "    geometry GEOMETRY,\n" +
                "    ELEV int\n" +
                ");\n\n";
        if(ParsedList.size() > 0)
        {
            for(int i=1; i<ParsedList.size(); i++)
            {
                String temp = ParsedList.get(0).get(0);
                if(ParsedList.get(0).get(0).equals("X"))
                {
                    String sqlTemp = "insert into PointSQL(geometry,ELEV) values(ST_GeomFromText('POINT("+ ParsedList.get(i).get(0) +" "+ ParsedList.get(i).get(1) +")'), "+ ParsedList.get(i).get(2) +");\n";
                    sqlCommand += sqlTemp;
                }
                else if(ParsedList.get(0).get(0).equals("the_geom:LineString"))
                {
                    String sqlTemp = "insert into LineSQL(geometry,ELEV) values(1, ST_GeomFromText('LINESTRING"+ ParsedList.get(i).get(0) +"'),"+ ParsedList.get(i).get(1) +");\n";
                    sqlCommand += sqlTemp;
                }
                else if(ParsedList.get(0).get(0).equals("the_geom:Polygon"))
                {
                    String sqlTemp = "insert into GeoSQL(geometry,ELEV) values(1, ST_GeomFromText('polygon"+ ParsedList.get(i).get(0) +"'),"+ ParsedList.get(i).get(1) +");\n";
                    sqlCommand += sqlTemp;
                }
            }
        }
        return sqlCommand;
    }

    public static void SaveQueryToFile(String text)
    {
        try
        {
            FileUtils.writeStringToFile(new File("sqlQuery.sql"), text);
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

}
